let w: unknown = 1;
w = "string";
w = {
    runANonExistenMethod: () => {
        console.log("I think therefor I am");
    }
}as { runANonExistenMethod: () => void }

if(typeof w == 'object' && w != null) {
    (w as {runANonExistenMethod: Function}).runANonExistenMethod();
}
